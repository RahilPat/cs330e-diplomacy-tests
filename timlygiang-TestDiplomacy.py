#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Tim Giang / Suren Bhakta
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_eval, diplomacy_read, diplomacy_calculate, diplomacy_print, diplomacy_convert, action_Dict, army

class TestDiplomacy (TestCase):
    def test_solve_1(self):
        r = StringIO("A NewYork Support B\nB Seattle Support C\nC Miami Move Austin\nD Austin Hold\nE Honululu Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A NewYork\nB Seattle\nC [dead]\nD [dead]\nE Honululu\n")
        
    def test_solve_2(self):
        r = StringIO("A NewYork Support E\nB Seattle Support D\nC Miami Move Seattle\nD Austin Hold\nE Honululu Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A NewYork\nB [dead]\nC [dead]\nD [dead]\nE Austin\n")      
        
    def test_solve_3(self):
        r = StringIO("A NewYork Support D\nB Seattle Support D\nC Miami Hold\nD Austin Hold\nE Honululu Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A NewYork\nB Seattle\nC Miami\nD Austin\nE [dead]\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

    