from io import StringIO
from unittest import main, TestCase
from Diplomacy import *

class TestDiplomacy(TestCase):
    
    # army
    def test_army_1(self):
        a = army("A", city("Vienna"), ["Hold"])
        self.assertEqual(a.name, "A")
        self.assertEqual(a.loc.name, "Vienna")
        self.assertEqual(a.action, ["Hold"])
        self.assertEqual(a.supported_by, set())
        self.assertEqual(a.isAlive, True)
    
    def test_army_2(self):
        b = army("B", city("Budapest"), ["Support", "A"])
        self.assertEqual(b.name, "B")
        self.assertEqual(b.loc.name, "Budapest")
        self.assertEqual(b.action, ["Support", "A"])
        self.assertEqual(b.supported_by, set())
        self.assertEqual(b.isAlive, True)
    
    def test_army_3(self):
        c = army("C", city("Trieste"), ["Move", "Vienna"])
        self.assertEqual(c.name, "C")
        self.assertEqual(c.loc.name, "Trieste")
        self.assertEqual(c.action, ["Move", "Vienna"])
        self.assertEqual(c.supported_by, set())
        self.assertEqual(c.isAlive, True)
    
    def test_army_4(self):
        d = army("D", city("Rome"), ["Support", "C"])
        d.supported_by.add(army("E", city("Rome"), ["Support", "D"]))
        d.supported_by.add(army("F", city("Rome"), ["Support", "D"]))
        d.supported_by.add(army("G", city("Rome"), ["Support", "D"]))
        self.assertEqual(d.find_strength(), 4) # Should it be 4 or 3?

    # city
    def test_city_1(self):
        v = city("Vienna")
        self.assertEqual(v.name, "Vienna")
        self.assertEqual(v.army, set())

    def test_city_2(self):
        v = city("Budapest")
        self.assertEqual(v.name, "Budapest")
        self.assertEqual(v.army, set())
    
    def test_city_2(self):
        v = city("Trieste")
        self.assertEqual(v.name, "Trieste")
        self.assertEqual(v.army, set())

    # diplomacy_read
    def test_read_0(self):
        s = StringIO("")
        a_dict, c_dict = diplomacy_read(s)
        self.assertEqual(a_dict, {})
        self.assertEqual(c_dict, {})

    def test_read_1(self):
        s = StringIO("A Vienna Hold\n")
        a_dict, c_dict = diplomacy_read(s)
        self.assertEqual(a_dict["A"].name, "A")
        self.assertEqual(a_dict["A"].loc.name, "Vienna")
        self.assertEqual(a_dict["A"].action, ["Hold"])
        self.assertEqual(a_dict["A"].supported_by, set())
        self.assertEqual(a_dict["A"].isAlive, True)

        self.assertEqual(c_dict["Vienna"].name, "Vienna")
        self.assertEqual(c_dict["Vienna"].army, set([a_dict["A"]]))
    
    def test_read_2(self):
        s = StringIO("B Budapest Support A\n")
        a_dict, c_dict = diplomacy_read(s)
        self.assertEqual(a_dict["B"].name, "B")
        self.assertEqual(a_dict["B"].loc.name, "Budapest")
        self.assertEqual(a_dict["B"].action, ["Support", "A"])
        self.assertEqual(a_dict["B"].supported_by, set())
        self.assertEqual(a_dict["B"].isAlive, True)

        self.assertEqual(c_dict["Budapest"].name, "Budapest")
        self.assertEqual(c_dict["Budapest"].army, set([a_dict["B"]]))
    
    def test_read_3(self):
        s = StringIO("C Trieste Move Vienna\n")
        a_dict, c_dict = diplomacy_read(s)
        self.assertEqual(a_dict["C"].name, "C")
        self.assertEqual(a_dict["C"].loc.name, "Trieste")
        self.assertEqual(a_dict["C"].action, ["Move", "Vienna"])
        self.assertEqual(a_dict["C"].supported_by, set())
        self.assertEqual(a_dict["C"].isAlive, True)

        self.assertEqual(c_dict["Trieste"].name, "Trieste")
        self.assertEqual(c_dict["Trieste"].army, set([a_dict["C"]]))

    # diplomacy_eval
    def test_eval_0(self):
        s = StringIO("")
        a_dict = diplomacy_eval(s)
        self.assertEqual(a_dict, {})

    def test_eval_1(self):
        s = StringIO("A Vienna Hold\n")
        a_dict = diplomacy_eval(s)
        self.assertEqual(a_dict["A"].name, "A")
        self.assertEqual(a_dict["A"].loc.name, "Vienna")
        self.assertEqual(a_dict["A"].action, ["Hold"])
        self.assertEqual(a_dict["A"].supported_by, set())
        self.assertEqual(a_dict["A"].isAlive, True)
    
    def test_eval_2(self):
        s = StringIO("A Vienna Hold\nB Budapest Support A\n")
        a_dict = diplomacy_eval(s)
        self.assertEqual(a_dict["A"].name, "A")
        self.assertEqual(a_dict["A"].loc.name, "Vienna")
        self.assertEqual(a_dict["A"].action, ["Hold"])
        self.assertIn(a_dict["B"], a_dict["A"].supported_by)
        self.assertEqual(a_dict["A"].isAlive, True)

        self.assertEqual(a_dict["B"].name, "B")
        self.assertEqual(a_dict["B"].loc.name, "Budapest")
        self.assertEqual(a_dict["B"].action, ["Support", "A"])
        self.assertEqual(a_dict["B"].supported_by, set())
        self.assertEqual(a_dict["B"].isAlive, True)

    def test_eval_3(self):
        s = StringIO("A Vienna Hold\nB Budapest Support A\nC Trieste Move Vienna\n")
        a_dict = diplomacy_eval(s)
        self.assertEqual(a_dict["A"].name, "A")
        self.assertEqual(a_dict["A"].loc.name, "Vienna")
        self.assertEqual(a_dict["A"].action, ["Hold"])
        self.assertIn(a_dict["B"], a_dict["A"].supported_by)
        self.assertEqual(a_dict["A"].isAlive, True)

        self.assertEqual(a_dict["B"].name, "B")
        self.assertEqual(a_dict["B"].loc.name, "Budapest")
        self.assertEqual(a_dict["B"].action, ["Support", "A"])
        self.assertEqual(a_dict["B"].supported_by, set())
        self.assertEqual(a_dict["B"].isAlive, True)

        self.assertEqual(a_dict["C"].name, "C")
        self.assertEqual(a_dict["C"].loc.name, "Vienna")
        self.assertEqual(a_dict["C"].action, ["Move", "Vienna"])
        self.assertEqual(a_dict["C"].supported_by, set())
        self.assertEqual(a_dict["C"].isAlive, False)
    
    def test_eval_4(self):
        s = StringIO("A Vienna Hold\nB Budapest Support A\nC Trieste Move Vienna\nD Rome Support C\n")
        a_dict = diplomacy_eval(s)
        self.assertEqual(a_dict["A"].name, "A")
        self.assertEqual(a_dict["A"].loc.name, "Vienna")
        self.assertEqual(a_dict["A"].action, ["Hold"])
        self.assertIn(a_dict["B"], a_dict["A"].supported_by)
        self.assertEqual(a_dict["A"].isAlive, False)

        self.assertEqual(a_dict["B"].name, "B")
        self.assertEqual(a_dict["B"].loc.name, "Budapest")
        self.assertEqual(a_dict["B"].action, ["Support", "A"])
        self.assertEqual(a_dict["B"].supported_by, set())
        self.assertEqual(a_dict["B"].isAlive, True)

        self.assertEqual(a_dict["C"].name, "C")
        self.assertEqual(a_dict["C"].loc.name, "Vienna")
        self.assertEqual(a_dict["C"].action, ["Move", "Vienna"])
        self.assertIn(a_dict["D"], a_dict["C"].supported_by)
        self.assertEqual(a_dict["C"].isAlive, False)

        self.assertEqual(a_dict["D"].name, "D")
        self.assertEqual(a_dict["D"].loc.name, "Rome")
        self.assertEqual(a_dict["D"].action, ["Support", "C"])
        self.assertEqual(a_dict["D"].supported_by, set())
        self.assertEqual(a_dict["D"].isAlive, True)
    
    # diplomacy_print
    def test_print_0(self):
        w = StringIO("")
        diplomacy_print({}, w)
        self.assertEqual(w.getvalue(), "")
    
    def test_print_1(self):
        w = StringIO("")
        diplomacy_print(diplomacy_eval(StringIO("A Vienna Hold\n")), w)
        self.assertEqual(w.getvalue(), "A Vienna\n")
    
    def test_print_2(self):
        w = StringIO("")
        diplomacy_print(diplomacy_eval(StringIO("A Vienna Hold\nB Budapest Support A\n")), w)
        self.assertEqual(w.getvalue(), "A Vienna\nB Budapest\n")
    
    def test_print_3(self):
        w = StringIO("")
        diplomacy_print(diplomacy_eval(StringIO("A Vienna Hold\nB Budapest Support A\nC Trieste Move Vienna\n")), w)
        self.assertEqual(w.getvalue(), "A Vienna\nB Budapest\nC [dead]\n")   
    
    # diplomacy_solve
    def test_solve_0(self):
        r = StringIO("")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")
    
    def test_solve_1(self):
        r = StringIO("A Vienna Hold\n")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Vienna\n")
    
    def test_solve_2(self):
        r = StringIO("A Vienna Hold\nB Budapest Support A\n")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Vienna\nB Budapest\n")
    
    def test_solve_3(self):
        r = StringIO("A Vienna Hold\nB Budapest Support A\nC Trieste Move Vienna\n")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Vienna\nB Budapest\nC [dead]\n")

if __name__ == '__main__': # pragma: no cover
    main()
