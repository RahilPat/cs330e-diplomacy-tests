#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0],  "A Madrid Hold")
    
    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0], "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        self.assertEqual(arr[2], "C London Support B")
    
    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0],  "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        

    def test_read_4(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0], "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        self.assertEqual(arr[2], "C London Support B")
        self.assertEqual(arr[3], "D Austin Move London")
        
    def test_read_5(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0], "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        self.assertEqual(arr[2], "C London Move Madrid")

    def test_read_6(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0], "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        self.assertEqual(arr[2], "C London Move Madrid")
        self.assertEqual(arr[3], "D Paris Support B")
    
    def test_read_7(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        arr = diplomacy_read(s)
        self.assertEqual(arr[0], "A Madrid Hold")
        self.assertEqual(arr[1], "B Barcelona Move Madrid")
        self.assertEqual(arr[2], "C London Move Madrid")
        self.assertEqual(arr[3], "D Paris Support B")

    # Tests added *********************

    def test_my_read_8(self):
        s ="A Madrid Hold\nB Barcelona Move Madrid\nC London Support A"
        line = diplomacy_read(s)
        self.assertEqual(line[0], "A Madrid Hold")
        self.assertEqual(line[1], "B Barcelona Move Madrid")
        self.assertEqual(line[2], "C London Support A")
    
    def test_my_read_9(self):
        s ="A Houston Hold\nB Barcelona Move LosAngeles\nC LosAngeles Move Houston"
        line = diplomacy_read(s)
        self.assertEqual(line[0], "A Houston Hold")
        self.assertEqual(line[1], "B Barcelona Move LosAngeles")
        self.assertEqual(line[2], "C LosAngeles Move Houston")
    
    def test_my_read_10(self):
        s ="A Houston Support C\nB Austin Hold\nC LosAngeles Move Austin\nD SanFracisco Support B"
        line = diplomacy_read(s)
        self.assertEqual(line[0], "A Houston Support C")
        self.assertEqual(line[1], "B Austin Hold")
        self.assertEqual(line[2], "C LosAngeles Move Austin")
        self.assertEqual(line[3], "D SanFracisco Support B")
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        result = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(result[0], "A Madrid")
        
    def test_eval_2(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B Madrid")
        self.assertEqual(result[2], "C London")
    
    def test_eval_3(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B [dead]")
    
    def test_eval_4(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B [dead]")
        self.assertEqual(result[2], "C [dead]")
        self.assertEqual(result[3], "D [dead]")
        
    def test_eval_5(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B [dead]")
        self.assertEqual(result[2], "C [dead]")

    def test_eval_6(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B Madrid")
        self.assertEqual(result[2], "C [dead]")
        self.assertEqual(result[3], "D Paris")
    
    def test_eval_7(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B [dead]")
        self.assertEqual(result[2], "C [dead]")
        self.assertEqual(result[3], "D Paris")
        self.assertEqual(result[4], "E Austin")

    # Tests added *********************

    def test_my_eval_8(self):
        result = diplomacy_eval(["A Chicago Hold", "B Barcelona Move SanFrancisco", "C Madrid Support A", "D Austin Move Chicago", "E Houston Move Chicago", "F SanFrancisco Hold","G NewYork Support E"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B [dead]")
        self.assertEqual(result[2], "C Madrid")
        self.assertEqual(result[3], "D [dead]")
        self.assertEqual(result[4], "E [dead]")
        self.assertEqual(result[5], "F [dead]")
        self.assertEqual(result[6], "G NewYork")       
    
    def test_my_eval_9(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Hold", "C London Hold", "D Chicago Hold" ])
        self.assertEqual(result[0], "A Madrid")
        self.assertEqual(result[1], "B Barcelona")
        self.assertEqual(result[2], "C London")
        self.assertEqual(result[3], "D Chicago")
    
    def test_my_eval_10(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"])
        self.assertEqual(result[0], "A [dead]")
        self.assertEqual(result[1], "B Madrid")
        self.assertEqual(result[2], "C [dead]")
        self.assertEqual(result[3], "D Paris")
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    # Tests added *********************

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Lewiston Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_9(self):
        r = StringIO("A Madrid Hold\nB Lewiston Move Chicago\nC LosAngeles Hold\nD Chicago Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC LosAngeles\nD [dead]\n")

    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Move Madrid\nD Paris Support B\nE Houston Move London\nF Austin Move Barcelona\nG Lewiston Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Madrid\nD Paris\nE London\nF [dead]\nG Lewiston\n")


    
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""