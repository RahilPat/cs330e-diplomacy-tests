#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # -----
    # diplomacy_solve()
    # -----

    def test_corner_case_1(self):  # single army
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        expected_result = "A Madrid\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_2(self):  # two armies battling
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_3(self):  # two armies battling one support
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\nC Paris Support A\n")
        w = StringIO()
        expected_result = "A Madrid\nB [dead]\nC Paris\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_4(self):  # three armies battling two support A, B
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\nC Madrid Move Madrid\nD Barcelona Support A\nE NewYork Support B\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\nC [dead]\nD Barcelona\nE NewYork\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_5(self):  # three armies battling two support A
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\nC Madrid Move Madrid\nD Barcelona Support A\nE NewYork Support A\n")
        w = StringIO()
        expected_result = "A Madrid\nB [dead]\nC [dead]\nD Barcelona\nE NewYork\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_6(self):  # two armies battling two supports A, B
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\nC Paris Support A\nD Barcelona Support B\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\nC Paris\nD Barcelona\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_7(self):  # three armies battling three supports
        r = StringIO(
            "A Madrid Hold\nB Madrid Move Madrid\nC Madrid Move Madrid\nD Barcelona Support A\nE NewYork Support B\nF Austin Support C\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\nC [dead]\nD Barcelona\nE NewYork\nF Austin\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_8(self):  # two armies battling invalidated support
        r = StringIO("A Madrid Hold\nB Madrid Move Madrid\nC Paris Support A\nD Barcelona Move Paris\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_9(self):  # two armies battling one good support one invalidated support
        r = StringIO(
            "A Madrid Hold\nB Madrid Move Madrid\nC Paris Support A\nD Barcelona Move Paris\nE NewYork Support A\n")
        w = StringIO()
        expected_result = "A Madrid\nB [dead]\nC [dead]\nD [dead]\nE NewYork\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

    def test_corner_case_10(self):  # moving to uninitialized city, supporting uninitialized army, hold initialized city
        r = StringIO("A Barcelona Move Madrid\nB Madrid Support C\nC Barcelona Hold\nD Barcelona Hold\n")
        w = StringIO()
        expected_result = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_result)

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
..........
----------------------------------------------------------------------
Ran 10 tests in 0.001s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          80      0     58      0   100%
TestDiplomacy.py      66      0      0      0   100%
--------------------------------------------------------------
TOTAL                146      0     58      0   100%
"""
