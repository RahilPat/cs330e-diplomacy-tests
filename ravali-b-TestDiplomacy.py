# Unit tests for Project 2: Diplomacy
# Ravali Bhavaraju, Shraddha Janga

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
from io import StringIO
from unittest import main, TestCase

#Failure cases: Valid tests that should fail because Diplomacy.py is not yet implemented

#Corner cases: edges of the cases (all move, all hold)

class TestDiplomacy(TestCase):


     #Test Read
    def test_read_1(self):
        s = StringIO("A Oakland Hold\nB PaloAlto Move SanDiego\nC LosAngeles Move Fresno\nD Fresno Hold\nE SanFrancisco Support C\nF Sacramento Support A")
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A Oakland Hold", "B PaloAlto Move SanDiego", "C LosAngeles Move Fresno", "D Fresno Hold", "E SanFrancisco Support C", "F Sacramento Support A"])

    def test_read_2(self):
        s = StringIO("A Oakland Hold\nB PaloAlto Move SanDiego\nC LosAngeles Move Fresno\nD Fresno Hold\nE SanFrancisco Support C\nF Sacramento Support A")
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A Oakland Hold", "B PaloAlto Move SanDiego", "C LosAngeles Move Fresno", "D Fresno Hold", "E SanFrancisco Support C", "F Sacramento Support A"])

    def test_read_3(self):
        s = StringIO("A Oakland Hold\nB PaloAlto Move SanDiego\nC LosAngeles Move Fresno\nD Fresno Hold\nE SanFrancisco Support C\nF Sacramento Support A")
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A Oakland Hold", "B PaloAlto Move SanDiego", "C LosAngeles Move Fresno", "D Fresno Hold", "E SanFrancisco Support C", "F Sacramento Support A"])

    #Test Eval
    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid'])
        self.assertEqual(v, 'A [dead]\nB [dead]')

    def test_eval_2(self):
        v = diplomacy_eval(['A Madrid Hold'])
        self.assertEqual(v, 'A Madrid')

    def test_eval_3(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'])
        self.assertEqual(v, 'A [dead]\nB Madrid\nC London')


    #Test Print
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]')

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB [dead]')
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]')

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'A [dead]\nB Madrid\nC London')
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London')
        

    # Test Solve
    def test_solve_1(self):
        r = StringIO(
            "A Oakland Hold\nB PaloAlto Move SanDiego\nC LosAngeles Move Fresno\nD Fresno Hold\nE SanFrancisco Support C\nF Sacramento Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A Oakland\nB SanDiego\nC Fresno\nD [dead]\nE SanFrancisco\nF Sacramento"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_2(self):
        r = StringIO(
            "A LosAngeles Move Fresno\nB Fresno Hold\nC Irvine Support A\nD Malibu Support A\nE Fremont Support B\nF Berkeley Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A [dead]\nB [dead]\nC Irvine\nD Malibu\nE Fremont\nF Berkeley"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_3(self):
        r = StringIO(
            "A SanDiego Move Paris\nB Vancouver Move SanDiego\nC Paris Move LosAngeles\nD LosAngeles Move Vancouver")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A Paris\nB SanDiego\nC LosAngeles\nD Vancouver"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_4(self):
        r = StringIO("A Columbus Hold\nB Albany Move Columbus\nC Indianapolis Move Columbus\nD Richmond Support B\nE Charleston Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A [dead]\nB [dead]\nC [dead]\nD Richmond\nE Charleston"
        self.assertEqual(w.getvalue(), answer)
        
    def test_solve_5(self):
        r = StringIO(
            "A Austin Hold\nB ElPaso Hold\nC Boston Hold\nD Lubbock Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A Austin\nB ElPaso\nC Boston\nD Lubbock"
        self.assertEqual(w.getvalue(), answer)

    

if __name__ == "__main__":  # pragma: no cover
    main()
    
    
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
..............
----------------------------------------------------------------------
Ran 14 tests in 0.002s

Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          58      2     42      3    95%   45, 49, 55->51
TestDiplomacy.py      67      0      0      0   100%
--------------------------------------------------------------
TOTAL                125      2     42      3    97%

OK

"""



